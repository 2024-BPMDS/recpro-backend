package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.bpm.modelingService.OntologyModelingBpmActivityService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.RecproAttributeInstanceDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.processInstance.ProcessInstanceDtoCreator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class RecproTaskDtoCreator {
    private OntologyModelingBpmActivityService ontologyActivityService;
    private RecproAttributeInstanceDtoCreator attributeDtoCreator;
    private ProcessInstanceDtoCreator processInstanceDtoCreator;
    public RecproTaskDto fromRecproTask(Task task, FilterResultItem resultItem) {
        RecproTaskDto result = this.fromRecproTask(task);
        result.setLikeliness(resultItem.getValue());
        result.setVisible(true);
        if (resultItem.getPosition() != null) {
            result.setPosition(resultItem.getPosition());
        }
        return result;
    }

    public RecproTaskDto fromRecproTask(Task task) {
        RecproTaskDto result = new RecproTaskDto();
        if (task.getActivityId() == null || task.getProcessInstanceId() == null || task.getProcessId() == null) {
            return result;
        }
        Activity activity = ontologyActivityService.getById(task.getActivityId());
        result.setId(task.getId());
        result.setTaskId(task.getTaskId());
        result.setAssigneeId(task.getAssigneeId());
        result.setActivity(activity);
        result.setProcessInstance(processInstanceDtoCreator.fromProcessInstanceId(task.getProcessInstanceId(), task.getProcessId()));
        result.setPriority(task.getPriority());
        result.setCreateDate(task.getCreateDate());
        result.setEditDate(task.getEditDate());
        result.setDueDate(task.getDueDate());
        result.setExecutionId(task.getExecutionId());
        result.setState(task.getState());
        result.setRecproElementId(task.getRecproElementId());
        result.setRecproElementInstanceId(task.getRecproElementInstanceId());
        result.setAttributes(attributeDtoCreator.fromRecproAttribute(activity.getAttributes(), task.getRecproElementId(), task.getRecproElementInstanceId(), task.getProcessInstanceId()));

        result.setLikeliness(null);
        result.setVisible(true);
        result.setPosition(-1);
        return result;
    }

    public List<RecproTaskDto> fromRecproTask(List<Task> tasks, List<FilterResultItem> resultItems) {
        return tasks.stream().map(task -> {
                FilterResultItem item = FilterResultItem.findByTaskId(task.getId(), resultItems);
                return this.fromRecproTask(task, item);
        }).toList();
    }

    public List<RecproTaskDto> fromRecproTask(List<Task> tasks) {
        return tasks.stream().map(this::fromRecproTask).toList();
    }
}
