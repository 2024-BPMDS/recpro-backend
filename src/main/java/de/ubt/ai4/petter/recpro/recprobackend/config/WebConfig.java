package de.ubt.ai4.petter.recpro.recprobackend.config;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private static final long MAX_AGE_SECS = 3600;

    @Value("${recpro.baseurl.dev}")
    String baseUrl;

    @Bean
    public WebMvcConfigurer corsDevConfigurer() {
        final String finalBaseUrl = this.baseUrl;
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(@NonNull CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(finalBaseUrl)
                        .allowedMethods("PUT", "DELETE", "GET", "POST", "OPTIONS").maxAge(MAX_AGE_SECS);
            }

        };
    }
}
