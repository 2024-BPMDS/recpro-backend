package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.base;

import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.service.IFilterExecutionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RecproBaseFilterExecutionService {

    private IFilterExecutionService filterExecutionServiceImpl;

}
