package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.similarActivity;

import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/recpro/filter/execution/similarActivity")
@AllArgsConstructor
public class SimilarActivityController {

    private final SimilarActivityService similarActivityService;

    @PostMapping("processInstance")
    public ResponseEntity<RecproFilterInstanceDto> processInstance(@RequestBody RecproTaskDto task) {
        return ResponseEntity.ok(similarActivityService.getByProcessId(task));
    }

}
