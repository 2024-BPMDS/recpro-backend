package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTasklistDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task.RecproTaskDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.RecproFilterExecutionService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.creatorServices.RecproFilterInstanceDtoCreator;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@Service

public class RecproTasklistDtoCreator {

    private RecproTaskDtoCreator taskDtoCreator;
    private RecproFilterExecutionService filterExecutionService;
    private RecproFilterInstanceDtoCreator filterInstanceDtoCreator;

    public RecproTasklistDto fromTasklist(Tasklist tasklist) {
        RecproTasklistDto result = new RecproTasklistDto();
        result.setId(tasklist.getId());
        result.setAssigneeId(tasklist.getAssigneeId());
        result.setDate(Instant.now());
        result.setTasks(taskDtoCreator.fromRecproTask(tasklist.getTasks()));
        result.setKnowledgeBasedFilter(filterInstanceDtoCreator.fromRecproFilterInstance(filterExecutionService.getByTasklistId(tasklist.getId(), tasklist.getAssigneeId(), FilterType.KNOWLEDGE_BASED)));
        result.setBaseFilter(filterInstanceDtoCreator.fromRecproFilterInstance(filterExecutionService.getByTasklistId(tasklist.getId(), tasklist.getAssigneeId(), FilterType.BASE)));
        return result;
    }

    public static RecproTasklistDto createTasklistDto(Tasklist tasklist, List<RecproTaskDto> tasks, RecproFilterInstanceDto knowledgeBasedFilterInstanceDto, RecproFilterInstanceDto baseFilterInstanceDto, RecproFilterInstanceDto filterInstanceDto) {
        RecproTasklistDto result = new RecproTasklistDto();
        result.setId(tasklist.getId());
        result.setAssigneeId(tasklist.getAssigneeId());
        result.setDate(Instant.now());
        result.setTasks(tasks);
        result.setKnowledgeBasedFilter(knowledgeBasedFilterInstanceDto);
        result.setBaseFilter(baseFilterInstanceDto);
        result.setFilter(filterInstanceDto);
        return result;

    }
}
