package de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto;

import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementInstance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor

@Getter
@Setter
public class RecproFilterRecproElementInstanceDto {
    private FilterBpmElementInstance instance;
}
