package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task;

import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproBinaryAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproNumericAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproTextualAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.task.TaskService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.AttributeExecutionService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model.AttributeInstanceDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class RecproTaskService {
    private TaskService taskService;
    private RecproTaskCreator taskCreatorService;
    private RecproTaskDtoCreator taskDtoCreatorService;
    private AttributeExecutionService attributeExecutionService;

    public RecproTaskDto claim(RecproTaskDto task, String assigneeId) {
        return taskDtoCreatorService.fromRecproTask(taskService.claim(taskCreatorService.fromRecproTaskDto(task), assigneeId));
    }

    public RecproTaskDto start(RecproTaskDto task) {
        return taskDtoCreatorService.fromRecproTask(taskService.start(taskCreatorService.fromRecproTaskDto(task)));
    }

    public RecproTaskDto complete(RecproTaskDto task) {
        attributeExecutionService.create(task.getAttributes());

        List<AttributeInstanceDto> attributeInstances = new ArrayList<>(task.getAttributes());
        attributeInstances = attributeInstances.stream().filter(attr -> !attr.getAttribute().getBpmsAttributeId().isEmpty()).toList();

        Map<String, Object> attributes = attributeInstances.stream().collect(Collectors.toMap(
                attr -> attr.getAttribute().getBpmsAttributeId(),
                attr ->
                    switch (attr.getInstance().getAttributeType()) {
                        case BINARY -> ((RecproBinaryAttributeInstance) attr.getInstance()).isValue();
                        case TEXT -> ((RecproTextualAttributeInstance) attr.getInstance()).getValue();
                        case NUMERIC -> ((RecproNumericAttributeInstance) attr.getInstance()).getValue();
                        default -> null;
                    }
                 ));

        taskService.setAttributes(task.getTaskId(), attributes);
        return taskDtoCreatorService.fromRecproTask(taskService.complete(taskCreatorService.fromRecproTaskDto(task)));
    }

    public RecproTaskDto unclaim(RecproTaskDto task) {
        return taskDtoCreatorService.fromRecproTask(taskService.unclaim(taskCreatorService.fromRecproTaskDto(task)));
    }
    public RecproTaskDto cancel(RecproTaskDto task) {
        return taskDtoCreatorService.fromRecproTask(taskService.cancel(taskCreatorService.fromRecproTaskDto(task)));
    }

    public RecproTaskDto stop(RecproTaskDto task) {
        return taskDtoCreatorService.fromRecproTask(taskService.stop(taskCreatorService.fromRecproTaskDto(task)));
    }

    public RecproTaskDto getLatestByUserId(String userId) {
        return taskDtoCreatorService.fromRecproTask(taskService.getLatestByUserId(userId));
    }
}
