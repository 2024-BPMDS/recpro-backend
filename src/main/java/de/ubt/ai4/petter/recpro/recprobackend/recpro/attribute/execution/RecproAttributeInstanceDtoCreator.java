package de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.service.AttributeInstanceExecutionService;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.attribute.AttributeModelingService;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.attribute.execution.model.AttributeInstanceDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class RecproAttributeInstanceDtoCreator {

    private AttributeModelingService attributeModelingService;
    private AttributeInstanceExecutionService attributeInstanceExecutionService;

    public AttributeInstanceDto fromRecproAttributeInstance(RecproAttributeInstance instance) {
        RecproAttribute attribute = attributeModelingService.getById(instance.getRecproAttributeId());
        return new AttributeInstanceDto(attribute, instance);
    }

    public List<AttributeInstanceDto> fromRecproAttributeInstance(List<RecproAttributeInstance> instances) {
        return instances.stream().map(this::fromRecproAttributeInstance).toList();
    }

    public AttributeInstanceDto fromRecproAttribute(RecproAttribute attribute, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        RecproAttributeInstance instance = attributeInstanceExecutionService.getByRecproAttribute(attribute, recproElementId, recproElementInstanceId, recproProcessInstanceId);
        return new AttributeInstanceDto(attribute, instance);
    }

    public List<AttributeInstanceDto> fromRecproAttribute(List<RecproAttribute> attributes, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        return  attributes.stream().map(attribute -> this.fromRecproAttribute(attribute, recproElementId, recproElementInstanceId, recproProcessInstanceId)).toList();
    }
}
