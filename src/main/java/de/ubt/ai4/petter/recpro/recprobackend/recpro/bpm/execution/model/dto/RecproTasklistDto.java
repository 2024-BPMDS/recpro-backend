package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto;

import de.ubt.ai4.petter.recpro.recprobackend.recpro.filter.execution.model.dto.RecproFilterInstanceDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproTasklistDto {
    private Long id;
    private String assigneeId;
    private List<RecproTaskDto> tasks;
    private RecproFilterInstanceDto filter;
    private RecproFilterInstanceDto knowledgeBasedFilter;
    private RecproFilterInstanceDto baseFilter;
    private Instant date;
}
