package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task;

import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTaskDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/recpro/bpm/execution/task-dto/")
@AllArgsConstructor
public class RecproTaskDtoController {
    private RecproTaskService taskService;

    @PostMapping("/claim")
    public RecproTaskDto claim(@RequestBody RecproTaskDto task, @RequestHeader("X-User-ID") String assigneeId ) {
        return taskService.claim(task, assigneeId);
    }

    @PostMapping("/start")
    public RecproTaskDto start(@RequestBody RecproTaskDto task) {
        return taskService.start(task);
    }

    @PostMapping("/cancel")
    public RecproTaskDto cancel(@RequestBody RecproTaskDto task ) {
        return taskService.cancel(task);
    }

    @PostMapping("/unclaim")
    public RecproTaskDto unclaim(@RequestBody RecproTaskDto task) {
        return taskService.unclaim(task);
    }


    @PostMapping("/complete")
    public RecproTaskDto complete(@RequestBody RecproTaskDto task) {
        return taskService.complete(task);
    }


    @PostMapping("/update")
    public void update() {
        // TODO document why this method is empty
    }

    @PostMapping("/stop")
    public RecproTaskDto stop(@RequestBody RecproTaskDto task) {
        return taskService.stop(task);
    }

    @GetMapping("/getLatestByUserId")
    public RecproTaskDto getLatestByUserId(@RequestHeader("X-User-ID") String userId) {
        return taskService.getLatestByUserId(userId);
    }
}
