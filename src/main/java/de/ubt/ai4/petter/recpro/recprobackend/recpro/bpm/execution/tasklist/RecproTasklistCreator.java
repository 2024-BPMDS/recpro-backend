package de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.model.dto.RecproTasklistDto;
import de.ubt.ai4.petter.recpro.recprobackend.recpro.bpm.execution.task.RecproTaskCreator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service
public class RecproTasklistCreator {
    private RecproTaskCreator taskCreatorService;

    public Tasklist fromRecproTasklistDto(RecproTasklistDto dto) {
        Tasklist result = new Tasklist();
        result.setId(null);
        result.setAssigneeId(dto.getAssigneeId());
        result.setTasks(taskCreatorService.fromRecproTaskDto(dto.getTasks()));
        result.setDate(dto.getDate());
        return result;
    }
}
